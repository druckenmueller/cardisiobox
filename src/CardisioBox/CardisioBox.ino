#define DEBUG false

#include <Dhcp.h> //  Ethernet
#include <Dns.h>
#include <Ethernet2.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp2.h>

#include <DueFlashStorage.h>  //  Saving non-volatile into flash
#include "efc.h"
#include "flash_efc.h"  //  Basics
#include <util.h>
#include <SPI.h>
#include <TimeLib.h>
#include <Wire.h>
#include "Base64.h"

#include "SdFat.h"  //  SD
#include "UserDataType.h"
#include "CircularBuffer.h"
#include "String.h"
#include "GlobalDefines.h"
#include <DS1307RTC.h>  //  RTC
#include "adsCommand.h" //  ads1298
#include "ads129x.h"

//  Defines so the device can do a self reset
#define SYSRESETREQ    (1<<2)
#define VECTKEY        (0x05fa0000UL)
#define VECTKEY_MASK   (0x0000ffffUL)
#define AIRCR          (*(uint32_t*)0xe000ed0cUL) // fixed arch-defined address
#define REQUEST_EXTERNAL_RESET (AIRCR=(AIRCR&VECTKEY_MASK)|VECTKEY|SYSRESETREQ)

#define REQ_BUF_SZ   50 //  http request capture
#define RES_BUF 3000

char req[REQ_BUF_SZ];

const int nRequestTemplates = 4;

const char* requestTemplates[4] = {
  "GET /52a5c69d51294a74e596b7ed0cfc7fdbd4956c70f63c243465854236f959cb90aa8626d751ecc177f52f9687b6b651444e6a992d2133fbd7bb32770fd0639d765b93461476ec3f8e997c5e011fa91e6f056e8cc63a4ac6b6c9093195cb23962499cde48c/start\0",
  "GET /52a5c69d51294a74e596b7ed0cfc7fdbd4956c70f63c243465854236f959cb90aa8626d751ecc177f52f9687b6b651444e6a992d2133fbd7bb32770fd0639d765b93461476ec3f8e997c5e011fa91e6f056e8cc63a4ac6b6c9093195cb23962499cde48c/stop\0",
  "GET /52a5c69d51294a74e596b7ed0cfc7fdbd4956c70f63c243465854236f959cb90aa8626d751ecc177f52f9687b6b651444e6a992d2133fbd7bb32770fd0639d765b93461476ec3f8e997c5e011fa91e6f056e8cc63a4ac6b6c9093195cb23962499cde48c/download/:filename\0",
  "GET /52a5c69d51294a74e596b7ed0cfc7fdbd4956c70f63c243465854236f959cb90aa8626d751ecc177f52f9687b6b651444e6a992d2133fbd7bb32770fd0639d765b93461476ec3f8e997c5e011fa91e6f056e8cc63a4ac6b6c9093195cb23962499cde48c/healthCheck\0"
};

boolean isRecording = false;

CircularBuffer <data_t, 1000> mybuff; //  ?

SdFat SD;
File dataFile;
data_t dataSet;

DueFlashStorage dueFlashStorage;  // used for non volatile filename in flash

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  //  Ethernet

IPAddress ip(192, 168, 178, 40); // IP address, may need to change depending on network
//IPAddress ip(192, 168, 8, 128); // IP address, may need to change depending on network
EthernetServer server(80);  // create a server at port 80
File webFile;



const int chipSelectSDcard = 4;
const int buttonInputPin = 12; //button connected between pin 12 and GND
const int ledPin = 13; //LED on board of Arduino Due
const int statePin = 2;
int reply = 0;
uint8_t serialBytes[200]; // todo: are 200 fields needed?
char sampleBuffer[1000]; // todo: are 1000 fields needed?
char timeBuffer[20];
char hexDigits[] = "0123456789ABCDEF";
char fileName[] = "00000000.TXT";
unsigned long datareadyTime = 0;
String patientParameters, patientData;
char HTTP_req[REQ_BUF_SZ] = {0}; // buffered HTTP request stored as null terminated string
char req_index = 0;              // index into HTTP_req buffer
int recording = 0;
int recordingold = 0;

void stateRising() 
{
  REQUEST_EXTERNAL_RESET;
}

void StrClear(char *str, char length) // sets every element of str to 0 (clears array)
{
  for (int i = 0; i < length; i++) {
    str[i] = 0;
  }
}

void dateTime(uint16_t* date, uint16_t* time) {
  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(year(), month(), day());

  // return time using FAT_TIME macro to format fields
  *time = FAT_TIME(hour(), minute(), second());
}

void updateFileName(void) {

  uint8_t filename_highbyte = dueFlashStorage.read(0); //reading from flash
  uint8_t filename_lowbyte = dueFlashStorage.read(1);
  uint16_t filename = ((filename_highbyte << 8 ) | filename_lowbyte);  //two bytes to one integer

  if (filename == 0) {
    filename = 10;  //last used filename, filename is 0 at first after each flashing
  }
  filename++; //next file´s name

  /* Prototyp location:
     A  KH Kamp-Lintford
     B  KH Du-Nord
     C  KH Meiderich
     D  Karina (Praktikantin)
     E
     F
     G
  */

  fileName[0] = 'Z';  //prototyp location
  fileName[1] = '0';  //program version
  fileName[2] = 'X';

  fileName[3] = filename % 100000 / 10000 + '0'; //
  fileName[4] = filename % 10000 / 1000 + '0';
  fileName[5] = filename % 1000 / 100 + '0';
  fileName[6] = filename % 100 / 10 + '0';
  fileName[7] = filename % 10 + '0';

  filename_lowbyte = filename; //integer into two bytes
  filename_highbyte = (filename >> 8 );

  dueFlashStorage.write(0, filename_highbyte);
  dueFlashStorage.write(1, filename_lowbyte);

  //guiSerial.print("File name: ");
  //guiSerial.println(fileName);

}

void encodeHex(char* output, char* input, int inputLen) {
  register int count = 0;
  for (register int i = 0; i < inputLen; i++) {
    register uint8_t lowNybble = input[i] & 0x0f;
    register uint8_t highNybble = input[i] >> 4;
    output[count++] = hexDigits[highNybble];
    output[count++] = hexDigits[lowNybble];
  }
  output[count] = 0;
}

inline void checkForSamples(void) {
  if ((!isRdatac) || (numActiveChannels < 1) )  return;
  if (digitalRead(drdyADS1298R) == HIGH) return;

  datareadyTime = micros();
  acquireData(&dataSet);
  mybuff.push(dataSet);
  if (mybuff.remain() >= 1000) guiSerial.println("ERRBUFF");
}

void writeHeader(void) {
  dataFile.print("time in us");
  dataFile.print("\t");
  dataFile.print("raw ADC data in base64 (4 characters per channel)");
  dataFile.print("\t");
  dataFile.print("Channel Gain");
  dataFile.print("\t");
  dataFile.print("Reference Voltage (in V)");
  dataFile.print("\t");
  dataFile.print(patientParameters);
  dataFile.println();

  dataFile.print("\t\t");
  switch (channelGain)
  {
    case ADS129x::GAIN_1X:
      dataFile.print("1");
      break;

    case ADS129x::GAIN_2X:
      dataFile.print("2");
      break;

    case ADS129x::GAIN_3X:
      dataFile.print("3");
      break;

    case ADS129x::GAIN_4X:
      dataFile.print("4");
      break;

    case ADS129x::GAIN_6X:
      dataFile.print("6");
      break;

    case ADS129x::GAIN_8X:
      dataFile.print("8");
      break;

    case ADS129x::GAIN_12X:
      dataFile.print("12");
      break;

    default:
      dataFile.print("error");
      break;
  }

  dataFile.print("\t");
  switch (referenceVoltage)
  {
    case ADS129x::VREF_4V:
      dataFile.print("4");
      break;

    case ADS129x::VREF_2V4:
      dataFile.print("2.4");
      break;

    default:
      dataFile.print("error");
      break;
  }

  dataFile.print("\t" + patientData);
  dataFile.println();
}

void saveSamples(void) {
  // check, if ring buffer is empty
  if (mybuff.remain() == 0) return;
  // create a time window for the starting time of a write process
  // assuming that the SD card needs less than 750 usec to write one sample
  if ( micros() - datareadyTime > 1250) return;
  //only save samples, if the SD card is not busy
  if ( SD.card()->isBusy() ) return;

  dataSet = mybuff.pop();
  base64_encode(sampleBuffer, (char *)dataSet.serialBytes, 8 * 3);

  //base64_encode(timeBuffer, (char *) &dataSet.time, 4);

  if (dataFile) {
    dataFile.print(dataSet.time);
    //dataFile.print(timeBuffer); // base64 encoding of time still has bugs
    dataFile.print("\t");
    dataFile.println(sampleBuffer);
    //dataFile.print(",");
    //dataFile.println(mybuff.remain());
  }
  // if the file isn't open, pop up an error:
  else {
    guiSerial.print("error opening ");
    guiSerial.println(fileName);
  }

}

char StrContains(const char *str,const char *sfind)
{
  char found = 0;
  char index = 0;
  char len;

  len = strlen(str);

  if (strlen(sfind) > len) {
    return 0;
  }
  while (index < len) {
    if (str[index] == sfind[found]) {
      found++;
      if (strlen(sfind) == found) {
        return 1;
      }
    }
    else {
      found = 0;
    }
    index++;
  }

  return 0;
}


void digitalClockDisplay() {
  // digital clock display of the time
  guiSerial.print(hour());
  printDigits(minute());
  printDigits(second());
  guiSerial.print(" ");
  guiSerial.print(day());
  guiSerial.print(" ");
  guiSerial.print(month());
  guiSerial.print(" ");
  guiSerial.print(year());
  guiSerial.println();
}

void printDigits(int digits) {
  // utility function for digital clock display: prints preceding colon and leading 0
  guiSerial.print(":");
  if (digits < 10)
    guiSerial.print('0');
  guiSerial.print(digits);
}


void setupArduino() {

  // define input pins
  pinMode(drdyADS1298R, INPUT);
  pinMode(buttonInputPin, INPUT_PULLUP);

  // define output pins and desired pin states
  digitalWrite(ledPin, LOW);
  pinMode(ledPin, OUTPUT);
  digitalWrite(startADS1298R, LOW);
  pinMode(startADS1298R, OUTPUT);
  digitalWrite(resetADS1298R, HIGH); //important to define the output as HIGH before setting it as an output. Otherwise, errors result.
  pinMode(resetADS1298R, OUTPUT);
  digitalWrite(chipselectADS1298R, HIGH);  // set the Slave Select Pins as outputs
  pinMode(chipselectADS1298R, OUTPUT);
  pinMode(10, OUTPUT);  // SlaveSelect Ethernet

  // initialize SPI:
  SPI.begin();

  // initialize Serial:
  guiSerial.begin(9600);
  //while (guiSerial.read() >= 0) {} //http://forum.arduino.cc/index.php?topic=134847.0
  //delay(200);  // Catch Due reset problem


  pinMode(statePin, INPUT);
  attachInterrupt(digitalPinToInterrupt(statePin), stateRising, RISING);
}

void setupRTC()  {
  while (!Serial) ; // wait until Arduino Serial Monitor opens
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  if (timeStatus() != timeSet)
    guiSerial.println("Unable to sync with the RTC");
  else
    guiSerial.println("RTC has set the system time");
}

void setupSDcard()
{
  // disable Ethernet chip
  digitalWrite(10, HIGH);

  guiSerial.print("Initializing SD card...");

  SdFile::dateTimeCallback(dateTime);


  // see if the card is present and can be initialized:
  if ( !SD.begin(chipSelectSDcard, SPI_HALF_SPEED) ) { //bug: SPI_FULL_SPEED does not work here
    guiSerial.println("Card failed, or not present");
    guiSerial.println("ERRSD");
    // don't do anything more:
    return;
  }
  guiSerial.println("card initialized.");
}

void ethernetsetup()
{
  Ethernet.begin(mac, ip);  // initialize Ethernet device
  server.begin();           // start to listen for clients
}

void setup() {
  setupArduino();
  setupADS129x();
  isRdatac = false;
  setupSDcard();
  ethernetsetup();
  
  delay(200);

  // read the device ID
  reply = adc_rreg(ADS129x::ID);

  guiSerial.println();
  guiSerial.print("ID: ");
  guiSerial.println(reply, BIN);

  reply = adc_rreg(ADS129x::CHnSET + 1);

  detectActiveChannels();
  guiSerial.print("Number of active Channels: ");
  guiSerial.println(numActiveChannels);

  reply = adc_rreg(ADS129x::CONFIG1);
  guiSerial.print("CONFIG1: ");
  guiSerial.println(reply, BIN);

  reply = adc_rreg(ADS129x::CONFIG2);
  guiSerial.print("CONFIG2: ");
  guiSerial.println(reply, BIN);

  reply = adc_rreg(ADS129x::CONFIG3);
  guiSerial.print("CONFIG3: ");
  guiSerial.println(reply, BIN);

  setupRTC();

  if (timeStatus() == timeSet) {
    digitalClockDisplay();
  } else {
    guiSerial.println("The time has not been set.  Please run the Time");
    guiSerial.println("TimeRTCSet example, or DS1307RTC SetTime example.");
    guiSerial.println();
    delay(4000);
  }
  digitalWrite(startADS1298R, HIGH);
  
  /* gui wird dank ethernet nicht mehr gebraucht
   *  
  guiSerial.println("Please enter patient data");
  guiSerial.println("PATDATA");

  while (!guiSerial.available()) {
    SysCall::yield();
  }

  while (1) {
    if (guiSerial.available()) {
      char c = guiSerial.read();
      if (c == '\n') break;
      patientParameters += c;
    }
  }

  while (!guiSerial.available()) {
    SysCall::yield();
  }

  //TODO: write the following lines as a function "readLine()"
  while (1) {
    if (guiSerial.available()) {
      char c = guiSerial.read();
      if (c == '\n') break;
      patientData += c;
    }
  }

  //guiSerial.print("Patient Parameters "); guiSerial.println(patientParameters);
  //guiSerial.print("Patient Data: "); guiSerial.println(patientData);
  */
}

char foo[5][20];
int ticks=0;
int ticksToCheckClient=1000000;

void loop() {

  if (isRecording) {
    checkForSamples();
    saveSamples();
  }

  if (ticks++ >= ticksToCheckClient) {
    ticks = 0;

    Serial.println("check incoming connection");

    // listen for incoming clients
    EthernetClient client = server.available();
    if (client) {
      Serial.println("new client");
      Serial.println("new client");
      int reqIdx = 0;
      // an http request ends with a blank line
      boolean currentLineIsBlank = true;
      while (client.connected()) {
        if (client.available()) {
          char c = client.read();
          Serial.write(c);
  
          if (reqIdx < (REQ_BUF_SZ - 1)) {
            req[reqIdx] = c;          // save HTTP request character
            reqIdx++;
          }
          else {
            req[reqIdx] = '\0';
          }
  
          
          
          //req[reqIdx++] = c;
          
          // if you've gotten to the end of the line (received a newline
          // character) and the line is blank, the http request has ended,
          // so you can send a reply
          if (c == '\n' && currentLineIsBlank) {
            Serial.println(reqIdx);
            Serial.println(req);
            handleRequest(client);
            //Serial.println(req[5]);
            /*
            if (req[5] == 'a') {
              doStart(client, foo, foo);
            }
            else if (req[5] == 'b') {
              doStop(client, foo, foo);
            }
            */
            
            break;
          }
          if (c == '\n') {
            // you're starting a new line
            currentLineIsBlank = true;
          } else if (c != '\r') {
            // you've gotten a character on the current line
            currentLineIsBlank = false;
          }
        }
      }
      // give the web browser time to receive the data
      delay(1);
      // close the connection:
      client.stop();
      Serial.println("client disconnected");
    }
  }
}

void handleRequest(EthernetClient client) {
 char paramKeys[5][20];
 char paramValues[5][20];
 
 Serial.print("request:        ");
 Serial.println(req);

 for (int i=0; i<nRequestTemplates;i++) {
  const char* tpl = requestTemplates[i];
  Serial.print("check template: ");
  Serial.println(tpl);

  bool match = doesTemplateMatch(tpl, paramKeys, paramValues);

  Serial.print("match: ");
  Serial.println(match);

  if (match) {
   if (i==0) { // /start
    doStart(client, paramKeys, paramValues);
   }
   else if (i==1) {// /stop
    doStop(client, paramKeys, paramValues);
   }
   else if (i==2) {// /downlaod
    doDownload(client, paramKeys, paramValues);
   }
   else if (i==3) {// /healthCheck
    doHealthCheck(client, paramKeys, paramValues);
   }
   break;
  }
 }
}


bool doesTemplateMatch(const char* tpl, char paramKeys[][20], char paramValues[][20]) {
 int tx = 0;
 int rx = 0;
 int px = 0;
 int curParam = -1;

 boolean methodRead = false;
 boolean match = true;
 boolean parsingParam = false;
 
 while (tpl[tx] != '\0' || parsingParam) {
  if (DEBUG) {
    Serial.print("tx: ");
    Serial.print(tx);
    Serial.print(" rx: ");
    Serial.print(rx);
    Serial.print(" tpl: ");
    Serial.print(tpl[tx]);
    Serial.print(" req: ");
    Serial.println(req[rx]);
  }
  
  if (req[rx] == ' ') {
    if (methodRead) { // HTTP method already parsed? Second blank is end of request
      Serial.println("blank detected on req");
      if (parsingParam) {
        paramValues[curParam][px++] = '\0';
      }
      match = parsingParam;
      break;
    }
    methodRead = true;
  }
  if (tpl[tx] == ':') {
    Serial.println("param start detected");
   tx++;
   parsingParam = true;
   curParam++;
   px = 0;
  }
  if (tpl[tx] != req[rx] && !parsingParam) {
   Serial.println("tpl req mismatch");
   match = false;
   break;
  }
  if (parsingParam) {
   paramKeys[curParam][px] = tpl[tx];
   paramValues[curParam][px] = req[rx];
   px++;
  }
  if (tpl[tx] == '/') {
   parsingParam = false;
  }
  if (!parsingParam) {
   tx++;
   rx++;
  }
  else {
   if (tpl[tx] != '/' && tpl[tx] != '\0') {
    tx++;
   }
   if (req[rx] != '/') {
    rx++;
   }
  }
 }

 Serial.println("parsing done");
 
 return match;
}

void doStart(EthernetClient client, char paramKeys[][20], char paramValues[][20]) {
 Serial.println("doStart");
 
 if(isRecording) {
  client.println("HTTP/1.1 412");
  client.println();
 }
 else {
  startrecording();
  
  client.println("HTTP/1.1 200");
  client.println();
  
  isRecording = true;
 }
}

void doStop(EthernetClient client, char paramKeys[][20], char paramValues[][20]) {
 Serial.println("doStop");
 
 if (isRecording) {
  stoprecording();
  
  client.println("HTTP/1.1 200");
  client.println("Content-Type: text/plain");
  client.println("Connection: close");
  client.println();
  client.println(fileName);
  
  isRecording = false;
 }
 else {
  client.println("HTTP/1.1 412");
  client.println("Connection: close");
  client.println();
 }
}

void doHealthCheck(EthernetClient client, char paramKeys[][20], char paramValues[][20]) {
 Serial.println("doHealthCheck");
 
 client.println("HTTP/1.1 200");
 client.println("Content-Type: text/plain");
 client.println("Connection: close");
 client.println();
 client.println("alive");
}

void doDownload(EthernetClient client, char paramKeys[][20], char paramValues[][20]) {
 Serial.println("doDownload");
 
 int fileNameParamIdx = getParamIndex(paramKeys, "filename");
 
 if (fileNameParamIdx >= 0) {
  const char* filename = paramValues[fileNameParamIdx];
   
  Serial.print("filename to download: ");
  Serial.println(filename);
  
  webFile = SD.open(filename);
  
  if (webFile) {
    client.println("HTTP/1.1 200");
    client.println("Content-Type: text/plain");
    client.println("Connection: close");
    client.println();

    char buf[RES_BUF];
    int bufIdx = 0;
      
    while (webFile.available()) {
      char c = webFile.read();
      
      buf[bufIdx++] = c;
      
      if (bufIdx == RES_BUF) {
        if (DEBUG) {
          Serial.print("buf full, sending: ");
          Serial.println(buf);
        }
        client.write(buf);
        bufIdx = 0;
      }
      //client.write(webFile.read());
      //Serial.write(webFile.read());
    }

    if (bufIdx > 0) {
      if (bufIdx < RES_BUF) {
        buf[bufIdx] = '\0';
      }
      client.write(buf);
    }
    Serial.println("file written to response");
    webFile.close();
  }
  else {
    client.println("HTTP/1.1 404");
    client.println("Connection: close");
    client.println();
  }
 }
}

int getParamIndex(char paramKeys[][20], const char* paramName) {
 for (int i=0; i<5; i++) {
  if (strcmp(paramKeys[i], paramName) == 0) {
   return i;
  }
 }

 return -1;
}

void stoprecording()
{
  dataFile.close();
  adc_send_command(ADS129x::SDATAC); //stop continuous data acqusisition
  digitalWrite(ledPin, LOW);
  isRdatac = false;
}

void startrecording()
{
  updateFileName();
  dataFile = SD.open( fileName , FILE_WRITE);
  writeHeader();
  adc_send_command(ADS129x::RDATAC); //start continuous data acqusisition
  digitalWrite(ledPin, HIGH);
  isRdatac = true;
}
