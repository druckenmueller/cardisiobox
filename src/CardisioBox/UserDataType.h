#ifndef UserDataType_h
#define UserDataType_h

struct data_t {
  unsigned long time;
  uint8_t serialBytes[24];
};
#endif  // UserDataType_h
