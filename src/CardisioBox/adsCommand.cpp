/* adsCommand.cpp
 *
 * send and receive commands from TI ADS129x chips. 
 *
 * Copyright (c) 2013 by Adam Feuer <adam@adamfeuer.com>
 * Copyright (c) 2012 by Chris Rorden
 * Copyright (c) 2012 by Steven Cogswell and Stefan Rado
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "adsCommand.h"
#include "Arduino.h"
#include <SPI.h>
#include "UserDataType.h"

#include "GlobalDefines.h"



// SPI settings for the communication with ADS1298R
SPISettings settingsADS1298R(28000000, MSBFIRST, SPI_MODE1);

int numActiveChannels = 0;
boolean isRdatac = true;
int maxChannels = 8;
boolean gActiveChan[9]; // reports whether channels 1..8 are active (gActiveChan[0] is not used)





void setupADS129x(){
  using namespace ADS129x;
  
  delay(500); //pause to provide ads129n enough time to boot up...
  // stop continuous data acquisition
  adc_send_command(SDATAC);

  // Enter high resolution mode with 500 samples per second
  adc_wreg(CONFIG1,HIGH_RES_500_SPS);

  adc_wreg(CONFIG2, INT_TEST);  // generate internal test signals
  
  //Configure right leg drive
  //FOR RLD: Power up the internal reference and wait for it to settle
  adc_wreg(CONFIG3, RLDREF_INT | PD_RLD | PD_REFBUF | referenceVoltage | CONFIG3_const);
  delay(150);

  //TODO: choose appropriate channels to contribute to the right leg drive
  adc_wreg(RLD_SENSP, 0x02);  // only use channel IN2P and IN2N
  adc_wreg(RLD_SENSN, 0x02);  // for the RLD Measurement


  adc_wreg(CHnSET + 1,SHORTED | PD_n); //disable this channel

  //report the used channels with desired gain
  adc_wreg(CHnSET + 2, ELECTRODE_INPUT | channelGain);
  adc_wreg(CHnSET + 3, ELECTRODE_INPUT | channelGain);
  adc_wreg(CHnSET + 4, ELECTRODE_INPUT | channelGain);
  adc_wreg(CHnSET + 5, ELECTRODE_INPUT | channelGain);
  adc_wreg(CHnSET + 6, ELECTRODE_INPUT | channelGain);
  
  /*
  //apply test signal to channels
  adc_wreg(CHnSET + 2, TEST_SIGNAL | channelGain);
  adc_wreg(CHnSET + 3, TEST_SIGNAL | channelGain);
  adc_wreg(CHnSET + 4, TEST_SIGNAL | channelGain);
  adc_wreg(CHnSET + 5, TEST_SIGNAL | channelGain);
  adc_wreg(CHnSET + 6, TEST_SIGNAL | channelGain);
  */

  adc_wreg(CHnSET + 7,SHORTED | PD_n); //disable this channel
  adc_wreg(CHnSET + 8,SHORTED | PD_n); //disable this channel
  
  /*
  // CHANNEL 1
  for (int i = 1; i <= 1; ++i) {
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_1X); //report this channel with x1 gain
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_12X); //report this channel with x12 gain
    //adc_wreg(CHnSET + i, TEST_SIGNAL | GAIN_12X); //create square wave
    // TODO: text Power Down Bit here
    adc_wreg(CHnSET + i,SHORTED); //disable this channel
  }

  //CHANNELS 2 to 6
  for (int i = 2; i <= 6; ++i) {
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_1X); //report this channel with x1 gain
    adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_6X); //report this channel with x6 gain
    //adc_wreg(CHnSET + i, TEST_SIGNAL | GAIN_6X); //create square wave
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_12X); //report this channel with x12 gain
    //adc_wreg(CHnSET + i, TEST_SIGNAL | GAIN_12X); //create square wave
    
    //adc_wreg(CHnSET + i,SHORTED); //disable this channel
  }

  //CHANNELS 4 to 8
  for (int i = 7; i <= 8; ++i) {
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_1X); //report this channel with x1 gain
    //adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_12X); //report this channel with x12 gain
    //adc_wreg(CHnSET + i, TEST_SIGNAL | GAIN_12X); //create square wave
    // TODO: text Power Down Bit here
    adc_wreg(CHnSET + i,SHORTED); //disable this channel
  }
  */
}



/*void wait_for_drdy(int interval)
{
	int i = 0;
	while (digitalRead(IPIN_DRDY) == HIGH) {
		if (i < interval) {
			continue;
		}
		i = 0;
	}
}*/

void adc_send_command(int cmd) {
  SPI.beginTransaction(settingsADS1298R);
  digitalWrite (chipselectADS1298R, LOW);
  SPI.transfer(cmd); 
  delayMicroseconds(1);
  digitalWrite (chipselectADS1298R, HIGH);
  SPI.endTransaction();
}

/*
void adc_send_command_leave_cs_active(int cmd) {
   digitalWrite(PIN_CS, LOW);
   spiSend(cmd);
}
*/


void adc_wreg(int reg, int val) {
   //see pages 40,43 of datasheet - 
  SPI.beginTransaction(settingsADS1298R);
  digitalWrite (chipselectADS1298R, LOW);
  SPI.transfer(ADS129x::WREG | reg);
  delayMicroseconds(2);
  SPI.transfer(0);	// number of registers to be read/written – 1
  delayMicroseconds(2);
  SPI.transfer(val);
  delayMicroseconds(1);
  digitalWrite (chipselectADS1298R, HIGH);
  SPI.endTransaction();
}


int adc_rreg(int reg){
  uint8_t out = 0;
  
  SPI.beginTransaction(settingsADS1298R);
  digitalWrite (chipselectADS1298R, LOW);
  SPI.transfer(ADS129x::RREG | reg); // Read register ID
  delayMicroseconds(2);
  SPI.transfer(0);
  delayMicroseconds(2);
  out = SPI.transfer(0);
  delayMicroseconds(1);
  digitalWrite (chipselectADS1298R, HIGH);
  SPI.endTransaction();

  return((int)out);   
}

uint8_t adc_spiRec(uint8_t* buf, size_t len) {
  SPI.beginTransaction(settingsADS1298R);
  digitalWrite (chipselectADS1298R, LOW);
  for (size_t i = 0; i < len; i++) {
    buf[i] = SPI.transfer(0XFF);
  }
  //is a delay needed here???
  digitalWrite (chipselectADS1298R, HIGH);
  SPI.endTransaction();
  return 0;
}

void acquireData (data_t* data) {
  SPI.beginTransaction(settingsADS1298R);
  digitalWrite (chipselectADS1298R, LOW);

  //check and discard the first 3 Byte, which are constant
  if( (SPI.transfer(0xFF) != 0xC0) || (SPI.transfer(0xFF) != 0x00) || (SPI.transfer(0xFF) != 0x00) ) guiSerial.println("Data acquisition error");
  
  data->time = micros();
  for (size_t i = 0; i < 24; i++) {
    data->serialBytes[i] = SPI.transfer(0XFF);
  }
  digitalWrite (chipselectADS1298R, HIGH);
  SPI.endTransaction();
}

void detectActiveChannels() {  //set device into RDATAC (continous) mode -it will stream data
  if (isRdatac) return; //we can not read registers when in RDATAC mode
  //guiSerial.println("Detect active channels: ");
  using namespace ADS129x; 
  numActiveChannels = 0;
  for (int i = 1; i <= maxChannels; i++) {
    delayMicroseconds(1); 
    int chSet = adc_rreg(CHnSET + i);
    gActiveChan[i] = ((chSet & 7) != SHORTED);
    if ( (chSet & 7) != SHORTED) numActiveChannels ++;   
  }
}
