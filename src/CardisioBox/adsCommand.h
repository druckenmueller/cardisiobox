/*
 * adsCommand.h
 *
 * Copyright (c) 2013 by Adam Feuer <adam@adamfeuer.com>
 * Copyright (c) 2012 by Chris Rorden
 * Copyright (c) 2012 by Steven Cogswell and Stefan Rado
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ADSCOMMAND_h
#define ADSCOMMAND_h

#include "Arduino.h"
#include "UserDataType.h"
#include "ads129x.h"

//#include <SPI.h>

// constant that defines the gain of the used channels
const int channelGain = ADS129x::GAIN_12X;
const int referenceVoltage = ADS129x::VREF_4V;

// enable global access to variables
extern int numActiveChannels;
extern boolean isRdatac;



// constants define pins on Arduino 
const int chipselectADS1298R = 52;
const int startADS1298R = 46;
const int resetADS1298R = 48;
const int drdyADS1298R = 45;



//function prototypes
void setupADS129x();
void adc_wreg(int reg, int val); //write register
void adc_send_command(int cmd); 
void adc_send_command_leave_cs_active(int cmd); 
int adc_rreg(int reg); //read register
uint8_t adc_spiRec(uint8_t* buf, size_t len);
void acquireData (data_t* data);
void detectActiveChannels() ;

#endif
